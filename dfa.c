#include <stdio.h>
#include <string.h>
#include "dfa.h"
#include "print.h"
#include <stdlib.h>
// Ukuran maksimum string bebas yang akan didefinisikan.
#define MAX_SIZE 1024
#define EVEN_BIGGER_MAX_SIZE 8192

// Mendefinisikan variable yang dibutuhkan.
char transitions[EVEN_BIGGER_MAX_SIZE];
char initial_state[MAX_SIZE];
char final_states[MAX_SIZE];

/* Menambahkan char baru diujung string. */
char add(char * s, char c) {
	int l = strlen(s);
	s[l] = c;
	// Menambahkan null terminator.
	s[l + 1] = '\0';
	return 1;
}

/* Menambahkan string baru diujung string. */
char append(char * str1, char * str2) {
	int l = strlen(str1);
	int i = 0;
	while ((i < strlen(str2)) && (add(str1, *(str2 + i)))) {
		i++;
	}
	// Menambahkan null terminator.
	str1[l + strlen(str2)] = '\0';
	return 1;
}

/* Mencari posisi sebuah string f di string str. */
int find(char * str, char * f) {
	char found = 0;
	int i = 0;
	while ((!found) && (str[i] != '\0')) {
		int j = 0;
		char equal = 1;
		// Mengecek kecocokan str f di posisi i.
		while ((equal) && (f[j] != '\0')) {
			if (str[i+j] == f[j]) {
				j++;
			} else {
				equal = 0;
			}
		}
		// Bila cocok maka telah di temukan string f.
		if (equal) {
			found = 1;
		}
		i++;
	}
	if (found) { // Bila string f ditemukan, maka posisinya adalah i -1.
		return i - 1;
	} else {
		return -1;
	}
}

char transit(char * state, char alphabet, char * next) {
	// Menerima panjang dari string state.
	int l = strlen(state);
	// Membuat string baru untuk mencari hasil transisi di transition table.
	// Bentuk string yang digunakan untuk mencari hasil transisi : <state>\><alphabet>
	char * s = (char*) malloc((l + 3) * sizeof(char));
	memcpy(s, state, l * sizeof(char));
	s[l] = '>';
	s[l + 1] = alphabet;
	// Menambahkan null terminator.
	s[l + 2] = '\0';
	int start = find(transitions, s) + l + 3;
	if (start >= l + 3) {
		int end = find(transitions + start, ",");
		// Mengembalikan next state.
		memcpy(next, transitions + start, end * sizeof(char));
		// Menambahkan null terminator.
		next[end] = '\0';
		// Membebaskan memory.
		free(s);
		print("%s", next);
		return 1;
	}
	return 0;
}

char transit_ext(char * state, char * str, char * result) {
	char error = 0; // Apabila terjadi masalah ketika transisi sedang terjadi.
	char * s = str; // Pointer dari str input
	while ((!error) && (s < strlen(str) + str)) {
		if (!transit(state, *s, result)) { // Terjadi error.
			// State tidak menemukan alphabet transisi.
			printf("State %s tidak memiliki transisi dengan alphabet %c.\n", state, *s);
			error = 1;
		} else { // Tidak terjadi error.
			state = result; // State sekarang merupakan hasil transisi state sebelumnya.
			s++; // Mengambil alphabet berikutnya.
		}
	}
	return !error;
}

void reg_trans(char * state, char alphabet, char * next_state) {
	// Menambahkan string baru kedalam transition table.
	// Bentuk string : '[state]>[alphabet]>[next_state],'
	append(transitions, state);
	add(transitions, '>');
	add(transitions, alphabet);
	add(transitions, '>');
	append(transitions, next_state);
	add(transitions, ',');
}

void reg_init(char * s) {
	memcpy(initial_state, s, strlen(s) * sizeof(char));
	initial_state[strlen(s)] = '\0';
}

void reg_final(char * s) {
	append(final_states, s);
	add(final_states, ',');
}

char is_final(char * state) {
	char query[MAX_SIZE];
	// Membentuk string query yang akan dicari pada string final_states
	memcpy(query, state, strlen(state) * sizeof(char));
	query[strlen(state)] = '\0';
	add(query, ',');
	// Mengecek keberadaan string '[state],' pada string final_states.
	int pos = find(final_states, query);
	return pos != -1;
}

char check(char * input) {
	char next[MAX_SIZE];
	if (transit_ext(initial_state, input, next)) { // Bila berhasil melakukan extended transition
		return is_final(next);
	}
	// Bila gagal maka string input tidak diterima.
	return 0;
}
