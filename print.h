#ifndef print_h
#define print_h

extern char print_trace;

/* Mencentak tulisan ke layar.
 * I.S. fmt terdefinisi, ... tidak harus terdefinisi.
 * F.S. pesan tercetak di layar.
 * */
 void print(char * fmt, ...);

 #endif