#ifndef dfa_h
#define dfa_h

// Mendeklarasi variable yang dibutuhkan.
// Mengandung transition table DFA dalam bentuk string.
extern char transitions[];
// Menyatakan final states yang ada.
extern char final_states[];
// Menyatakan initial state.
extern char initial_state[];
// Bila 1, maka akan mencetak hasil transition function.
extern char print_trace;

/* Transition function yang mengembalikan nama state berikutnya.
 * I.S. state dan alphabet terdefinisi.
 * F.S. next berisi nama dari hasil transisi state oleh alphabet.
 * */
char transit(char * state, char alphabet, char * next);

/* Extended transition function yang menerima string.
 * I.S. state dan str terdefinisi.
 * F.S. result berisi nama dari hasil transisi state oleh setiap alphabet di str.
 * */
char transit_ext(char * state, char * str, char * result);

/* Menambahkan transition ke dalam transition table. 
 * I.S. state, alphabet dan next_state terdefinisi.
 * F.S. transition table mengandung nilai transisi dari state, alphabet menuju ke next_state.
 * */
void reg_trans(char * state, char alphabet, char * next_state);

/* Mendaftarkan sebuah state sebagai initial state.
 * I.S. state terdefinisi.
 * F.S. initial_state menjadi state tersebut.
 * */
void reg_init(char * state);

/* Mendaftarkan sebuah state sebagai salah satu dari final state. 
 * I.S. state terdefinisi.
 * F.S. state menjadi salah satu dari final state yang ada.
 * */
void reg_final(char * state);

/* Mengecek apabila sebuah state merupakan final state.
 * I.S. state terdefinisi.
 * F.S. mengembalikan nilai 1 bila state merupakan final state, 0 bila bukan.
 * */
char is_final(char * state);

/* Mengecek apabila suatu string input diterima. 
 * I.S. input terdefinisi.
 * F.S. mengembalikan 1 bila string input diterima, 0 bila tidak.
 * */
char check(char * input);

/* Mencari posisi sebuah string di string.
 * I.S. str dan f terdefinisi.
 * F.S. mengembalikan nilai posisi string f di string str.
 * */
int find(char * str, char * f);

/* Menambahkan string di akhir string lainnya.
 * I.S. str1 dan str2 terdefinisi.
 * F.S. str1 berubah terkonkatenasi menjadi str1..str2
 * */
char append(char * str1, char * str2);

/* Menambahkan char di akhir string.
 * I.S. s dan c terdefinisi.
 * F.S. char terakhir string s adalah c.
 * */
char add(char * s, char c);

#endif

