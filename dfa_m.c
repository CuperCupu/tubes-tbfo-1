#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "dfa.h"
#include "print.h"
#define TAG_ALIAS "@alias"
#define TAG_INITIAL "@initial"
#define TAG_FINALS "@finals"
#define TAG_TRANSITION "@transition"
#define MAX_SIZE 2048

char aliases[MAX_SIZE];

/* Mengembalikan alias dari sebuah string.
 * I.S. string terdefinisi di aliases.
 * F.S. mengembalikan alias dari string.
 * */
char * get_alias(char * string) {
	char * str = (char *) malloc((strlen(string) + 1) * sizeof(char));
	memcpy(str, string, sizeof(char) * strlen(string));
	str[strlen(string)] = '\0';
	add(str, '=');
	int start = find(aliases, str);
	free(str);
	if (start != -1) {
		start += strlen(string) + 1;
		int len = find(aliases + start, ",");
		if (len != -1) {
			char * result = (char *) malloc((len + 1) * sizeof(char));
			memcpy(result, aliases + start, len * sizeof(char));
			result[len] = '\0';
			return result;
		}
		return NULL;
	}
	return NULL;
}

/* Mendaftarkan alias dari sebuah string.
 * I.S. string dan chr terdefinisi.
 * F.S. chr terdaftar sebagai alias dari string.
 * */
void reg_alias(char * string, char * chr) {
	append(aliases, string);
	add(aliases, '=');
	append(aliases, chr);
	add(aliases, ',');
}

/* Menerima sebuah string dan mendaftarkan alias pada string tersebut.
 * I.S. string dan length(panjang dari string tersebut) terdefinisi.
 * F.S. alias terdaftar.
 * */
char parse_aliases(char * string, long length) {
	// Mencari posisi TAG_ALIAS pada string.
	int start = find(string, TAG_ALIAS);
	if (start != -1) {
		// Berpindah ke baris berikutnya.
		start = start + find(string + start, "\n") + 1;
		while ((start < length) && (string[start] != '@')) { // Menerima setiap baris sebagai alias.
			// Memindai panjang dari baris saat ini.
			int end = find(string + start, "\n");
			// Hanya membaca baris bila panjang dari baris lebih dari 4 (termasuk \n karena panjang minimum suatu data alias adalah 3 char).
			if (end > 4) {
				int len = find(string + start, " ");
				// Membaca string utama.
				char * str = (char *) malloc((len + 1) * sizeof(char));
				memcpy(str, string + start, sizeof(char) * (len));
				str[len] = '\0';
				// Membaca char alias.
				char * s = (char *) malloc((end - len) * sizeof(char));
				memcpy(s, string + start + len + 1, sizeof(char) * (end - len - 1));
				s[end - len - 1] = '\0';
				// Mendaftarkan char sebagai alias dari string.
				reg_alias(str, s);
				// Dealokasi memory.
				free(str);
				free(s);
				// Berpindah ke baris berikutnya.
			}
			start = start + end + 1;
		}
		return 1;
	} else {
		printf("Aliases not found.\n");
		return 0;
	}
}

/* Menerima sebuah string dan mendaftarkan final states yang tertera pada string.
 * I.S. string dan length(panjang dari string tersebut) terdefinisi.
 * F.S. final states terdaftarkan dengan fungsi reg_final oleh DFA machine.
 * */
char parse_finals(char * string, long length) {
	// Mencari posisi TAG_FINALS pada string.
	int start = find(string, TAG_FINALS);
	if (start != -1) {
		// Berpindah ke baris berikutnya.
		start = start + find(string + start, "\n") + 1;
		while ((start < length) && (string[start] != '@')) {
			// Memindai panjang dari baris saat ini.
			int len = find(string + start, "\n");
			// Hanya mendaftarkan konten dari baris saat ini apabila bukan string kosong(\n termasuk dalam panjang).
			if (len > 1) {
				// Membaca baris saat ini.
				char * str = (char *) malloc((len + 1) * sizeof(char));
				memcpy(str, string + start, sizeof(char) * (len));
				str[len] = '\0';
				// Mendaftarkan final state.
				char * a_str = get_alias(str);
				if (a_str != NULL) {
					reg_final(a_str);
					free(a_str);
				} else {
					reg_final(str);
				}
				// Dealokasi memory.
				free(str);
			}
			// Berpindah ke baris berikutnya.
			start = start + len + 1;
		}
		return 1;
	} else {
		printf("Final states not found.\n");
		return 0;
	}
}

/* Menerima serbuah string dan medaftarkan initial state.
 * I.S. string buffer terdefinisi.
 * F.S. initial state terdaftarkan.
 * */
char parse_initial(char * buffer) {
	int start = find(buffer, TAG_INITIAL);
	// Mencari TAG_INITIAL pada string.
	if (start != -1) {
		// Mencari akhir line.
		int end = find(buffer + start, "\n");
		// Menghitung panjang nama initial state.
		int len = end - find(buffer + start, " ");
		// Mengambil string initial state.
		char * init = (char *) malloc((len) * sizeof(char));
		memcpy(init, buffer + start + len + 1, (end - len - 1) * sizeof(char));
		// Menambahkan null terminator.
		init[len - 1] = '\0';
		// Mencari alias dari initial state.
		char * a_init = get_alias(init);
		if (a_init != NULL) {
			// Register alias.
			reg_init(a_init);
			// Dealokasi string alias.
			free(a_init);
		} else {
			reg_init(init);
		}
		// Dealokasi string initial state.
		free(init);
		return 1;
	} else {
		printf("Initial state not found.\n");
		return 0;
	}
}

/* Menerima sebuah string dan mendaftarkan seluruh transisi yang ada.
 * I.S. string dan length terdefinisi.
 * F.S. state transition terdefinisi.
 * */
char parse_transition(char * string, long length) {
	// Mencari TAG_TRANSITON di string.
	int start = find(string, TAG_TRANSITION);
	if (start != -1) {
		// Mencari posisi awal dari alphabets.
		start = start + find(string + start, " ") + 1;
		// Mencari akhir dari baris.
		int len = find(string + start, "\n");
		// Mengalokasi string untuk alphabet yang terdefinisi.
		char * alphabets = (char *) malloc((len / 2 + 1) * sizeof(char));
		int i = 0, j = 0;
		// Mengambil setiap alphabet dan menyimpan ke alphabets.
		while (i < len) {
			alphabets[j] = string[start + i];
			j++;
			i += 2;
		}
		// Menambahkan null terminator.
		alphabets[j] = '\0';
		// Lanjut ke baris berikutnya.
		start = start + len + 1;
		// Selama belum mencapai akhir string dan tidak menemukan tag lain.
		while ((start < length) && (string[start] != '@')) {
			// Mencari panjang string nama state asal.
			len = find(string + start, " ");
			if (len != -1) {
				// Menyimpan string nama state asal.
				char * state = (char *) malloc((len + 1) * sizeof(char));
				memcpy(state, string + start, len * sizeof(char));
				// Menambahkan null terminator.
				state[len] = '\0';
				// Berpindah ke kata berikut.
				start = start + len + 1;
				// Mencari akhir dari baris.
				int k = find(string + start, "\n");
				if (k == -1) { // Bila sudah berada di akhir string.
					// Posisi akhir string.
					k = length - start;
				}
				int end = start + k;
				j = 0;
				while (start < end) { // Iterasi untuk setiap kata di baris.
					// Menghitung panjang string nama state berikut.
					len = find(string + start, " ");
					// Bila berada di ujung baris.
					if (len == -1) {
						len = find(string + start, "\n");
					}
					// Bila sudah diakhir baris.
					if (len == -1) {
						start = length;
					} else {
						// Bila spasi yang ditemukan bukan di baris saat ini.
						if (start + len > end) {
							// Membatasi panjang string.
							len = end - start;
						}
						// Menyimpan string nama next state.
						char * n_state = (char *) malloc((len + 1) * sizeof(char));
						memcpy(n_state, string + start, len * sizeof(char));
						// Menambahkan null terminator.
						n_state[len] = '\0';
						// Mengambil alias state sekarang.
						char * a_state = get_alias(state);
						// Mengambil alias state berikutnya.
						char * a_n_state = get_alias(n_state);
						// Bila alias kedua state di temukan.
						if ((a_state != NULL) && (a_n_state != NULL)) {
							// Mendaftarkan transition state asal dan berikutnya sebagai alias dengan alphabet
							reg_trans(a_state, alphabets[j], a_n_state);
							// Dealokasi alias.
							free(a_state);
							free(a_n_state);
						} else {
							// Mendaftarkan transition state asal dan berikutnya dengan alphabet
							reg_trans(state, alphabets[j], n_state);
						}
						// Dealokasi string state berikutnya.
						free(n_state);
						// Sambung ke kata berikutnya.
						start = start + len;
					}
					// Mengabaikan spasi dan new line setelah baris.
					while (((string[start] == ' ') || (string[start] == '\n')) && (start < length)) {
						start++;
					}
					// Iterasi untuk alphabet berikut.
					j++;
				}
				// Dealokasi string nama state asal.
				free(state);
			} else {
				start = length;
			}

		}
		// Dealokasi alphabets.
		free(alphabets);
		return 1;
	} else {
		return 0;
	}
}

/* Menerima sebuah nama file dan mendaftarkan dfa machine yang terdapat di dalam.
 * I.S. filename terdefinisi.
 * F.S. bila file ada maka initial state, final states dan transition akan terdefinisi.
 *      bila tidak maka tidak terjadi apa-apa.
 * */
char parse(char * filename) {
	// Membuka file dari filename.
	FILE * f = fopen(filename, "r");
	// Bila file ditemukan.
	if (f != NULL) {
		// Deklarasi buffer.
		char * buffer;
		long length;
		// Menentukan panjang file (dalam char).
		fseek(f, 0, SEEK_END);
		length = ftell(f);
		fseek(f, 0, SEEK_SET);
		// Mengalokasi buffer untuk file.
		buffer = (char *) malloc((length + 1) * sizeof(char));
		if (buffer) { // Bila alokasi berhasil.
			char line[MAX_SIZE] = "\0"; // Buffer untuk membaca satu baris.
			while (fgets(line, sizeof(line), f) != NULL) {
				// Menambahkan baris yang terbaca kedalam buffer file.
				append(buffer, line);
			}
			// Menambahkan null terminator.
			buffer[length] = '\0';
			// Parse aliases, initial state, final states dan transition dari buffer file.
			parse_aliases(buffer, length);
			parse_initial(buffer);
			parse_finals(buffer, length);
			parse_transition(buffer, length);
			// Dealokasi buffer file.
			free(buffer);
		}
		// Menutup file.
		fclose(f);
		return 1;
	} else {
		printf("File %s tidak ditemukan.\n", filename);
		return 0;
	}
}

char print_trace = 0;

/* Mencentak tulisan ke layar.
 * I.S. fmt terdefinisi, ... tidak harus terdefinisi.
 * F.S. pesan tercetak di layar.
 * */
void print(char * fmt, ...) {
	if (print_trace) {
		// ALokasi 1KiB untuk output.
		char str[1024];
		// Format string
		va_list vl;
		va_start(vl, fmt);
		vsnprintf(str, sizeof(str), fmt, vl);
		va_end(vl);

		// Menampilkan alias.
		char * a_str = get_alias(str);
		if (a_str != NULL) {
			printf("%s\n", a_str);
			free(a_str);
		} else {
			printf("%s\n", str);
		}
	}
}

int main() {
	// Menerima nama file dfa.
	char input[MAX_SIZE];
	printf("Nama file: ");
	scanf("%s", input);
	if (parse(input)) { // Bila berhasil di ubah ke dfa.
		char * result[MAX_SIZE];
		// Memulai state dengan initial state.
		memcpy(result, initial_state, strlen(initial_state) * sizeof(char));
		print_trace = 1;
		// Menerima string aksi-aksi.
		printf("Aksi:\n");
		scanf("%s", input);
		char string[MAX_SIZE];
		// Mengubah string tersebut menjadi urutan alphabet.
		int c = 0;
		int pos = 0;
		int f = 0;
		// Apabila string masih valid.
		char valid = 1;
		do {
			f = find(input + pos, ",");
			if (f == -1) {
				f = strlen(input) - pos;
			}
			if (f != -1) {
				char * str = (char *) malloc(sizeof(char) * (f + 1));
				memcpy(str, input + pos, f * sizeof(char));
				str[f] = '\0';
				char * als = get_alias(str);
				if (als != NULL) {
					string[c] = als[0];
					c++;
				} else {
					printf("%s bukanlah aksi yang valid.\n", str);
					valid = 0;
				}
				free(als);
				free(str);
				pos = pos + f + 1;
			}
		} while ((f != -1) && (valid));
		string[c] = '\0';
		if (valid) {
			// Mengecek apabila string diterima.
			char acc = check(string);
			if (acc) {
				printf("Strings accepted\n");
			} else {
				printf("Strings rejected\n");
			}
		}
	}
	return 0;
}
