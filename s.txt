@alias
open_s 1
open_c 2
bind 3
listen 4
accept 5
connect 6
read_s 7
write_s 8
read_c 9
write_c a
close_s b
close_c c
initial ini
server_socket ss
client_socket cs
both_socket bs
server_binded sb
server_listening sl
server_accepting sa
both_s_binded bb
both_s_listening bl
both_s_accepting ba
idling idl
server_pending spe
client_pending cpe
both_pending bpe
server_polling spo
client_polling cpo
both_polling bpo
server_both sbo
client_both cbo
closed cld
aborted abt
ini Initial
ss Server socket is opened
cs Client socket is opened
bs Both sockets are opened
sb Server port is binded
sl Server is listening
sa Server is accepting
bb Server port is binded and client socket is opened
bl Server is listening and client socket is opened
ba Server is accepting and client socket is opened
idl Server is idling
spe Server message pending
cpe Client message pending
bpe Server and client messages pending
spo Server is polling messages
cpo Client is polling messages
bpo Server and client are polling messages
sbo Server is both pending and polling messages
cbo Client is both pending and polling messages
cld Closed
abt Aborted

@initial initial

@finals
server_socket
client_socket
both_socket
server_binded
server_listening
server_accepting
both_s_binded
both_s_listening
both_s_accepting
idling
server_pending
client_pending
both_pending
server_polling
client_polling
both_polling
server_both
client_both
closed

@transition 1 2 3 4 5 6 7 8 9 a b c
initial server_socket client_socket aborted aborted aborted aborted aborted aborted aborted aborted aborted aborted
server_socket server_socket both_socket server_binded aborted aborted aborted aborted aborted aborted aborted closed aborted
client_socket both_socket client_socket aborted aborted aborted aborted aborted aborted aborted aborted aborted closed
both_socket both_socket both_socket both_s_binded aborted aborted aborted aborted aborted aborted aborted client_socket server_socket
server_binded server_socket both_s_binded server_binded server_listening aborted aborted aborted aborted aborted aborted closed aborted
server_listening server_socket both_s_listening aborted server_listening server_accepting aborted aborted aborted aborted aborted closed aborted
server_accepting aborted both_s_accepting aborted aborted aborted idling aborted aborted aborted aborted closed aborted
both_s_binded both_s_binded both_s_binded both_s_binded both_s_listening aborted aborted aborted aborted aborted aborted client_socket server_binded
both_s_listening both_s_listening both_s_listening aborted both_s_listening both_s_accepting aborted aborted aborted aborted aborted client_socket server_listening
both_s_accepting aborted aborted aborted aborted aborted idling aborted aborted aborted aborted client_socket server_accepting
idling aborted aborted aborted aborted aborted aborted server_polling server_pending client_polling client_pending client_socket server_socket
server_pending aborted aborted aborted aborted aborted aborted server_both server_pending idling both_pending client_socket server_socket
client_pending aborted aborted aborted aborted aborted aborted idling both_pending client_both client_pending client_socket server_socket
both_pending aborted aborted aborted aborted aborted aborted server_pending both_pending client_pending both_pending client_socket server_socket
server_polling aborted aborted aborted aborted aborted aborted server_polling server_both both_polling idling client_socket server_socket
client_polling aborted aborted aborted aborted aborted aborted both_polling idling client_polling client_both client_socket server_socket
both_polling aborted aborted aborted aborted aborted aborted both_polling server_polling both_polling client_polling client_socket server_socket
server_both aborted aborted aborted aborted aborted aborted server_both server_both server_polling server_pending client_socket server_socket
client_both aborted aborted aborted aborted aborted aborted client_polling client_pending client_both client_both client_socket server_socket
closed server_socket client_socket aborted aborted aborted aborted aborted aborted aborted aborted aborted aborted
aborted aborted aborted aborted aborted aborted aborted aborted aborted aborted aborted aborted aborted
